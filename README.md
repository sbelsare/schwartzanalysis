# README #

This is the basic readme for this project

This is the first iteration of the code required for the Schwartz analysis of protein residue dynamics (ref: Mincer J. S and Schwartz J. D., A Computational Method to Identify Residues Important in Creating a Protein Promoting
Vibration in Enzymes J. Phys. Chem. B 2003, 107, 366-371) The codes are basic C++ and python. 

### What is this repository for? ###

* Performing the analysis outlined in Schwartz et al 
* Version 0.0.1

### How do I get set up? ###

* Download and run with the existing C++ or python compilers as required.

### Who do I talk to? ###

* Saurabh Belsare (smbelsare@gmail.com) 
