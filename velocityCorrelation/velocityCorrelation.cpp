/*
 Calculating the velocity correlation functions.
 This code will calculate the velocity correlation function between velocities of residues from the mdvel files. 
*/

// Includes
#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <time.h>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#define _USE_MATH_DEFINES
int count0 = 0, count1 = 0, count2 = 0;


using namespace std;

// Defining the functions.

void readInArray(double*** myArray, const char* dataFileName, int nAtom)
    {
    int i = 0, j = 0, k = 0, prevLen = 0;
    string line;
    ifstream dataFile ( dataFileName );
    ofstream testOutFile( "/Users/Saurabh/Desktop/testOutputFile.txt" );

    if ( dataFile.is_open() )
      {
      while ( dataFile.good() )
        {
        getline (dataFile,line);
        //testOutFile << line << endl;
        vector<string> strs;
        boost::trim(line);
        boost::split(strs, line, boost::is_any_of(" "), boost::token_compress_on);

        if ( strs.size() == 10 || prevLen == 10 )
          {
          for (int l = 0; l < strs.size(); l++)
            {
            testOutFile << "i = " << i << " j = " << j << " k = " << k << endl;
            myArray[i][j][k] = atof( strs[l].c_str() );
            k++;
            if ( k == 3 )
              {
              j++;
              k = 0;
              }
            if ( j > 0 && j % nAtom == 0 )
              {
              j = 0;
              i++;
              }
            }
          }
        prevLen = strs.size();
        strs.clear();
        }
      dataFile.close();
      testOutFile.close();
      }

    else
      {
      cout << "Unable to open file";
      dataFile.close();
      }
    }


// This function calculates the PPV
double calculatePPV(double*** velocityArray, double*** coordinatesArray, int a, int d, int t)
  {
  double PPV = 0, normalizingFactor = 0;
  double* Vda = new double[3];
  double* Rda = new double[3];
  double* Uda = new double[3];

  for (int i = 0; i < 3; i ++)
    {
    Vda[i] = velocityArray[t][d][i] - velocityArray[t][a][i];
    Rda[i] = coordinatesArray[t][d][i] - coordinatesArray[t][a][i];
    normalizingFactor += Rda[i] * Rda[i];
    }

  normalizingFactor = sqrt(normalizingFactor);

  for (int i = 0; i < 3; i++)
    {
    Uda[i] = Rda[i] / normalizingFactor;
    }

  for (int i = 0; i < 3; i++)
    {
    PPV += Vda[i] * Uda[i];
    }

  return PPV;
  }



// This function calculates the center of mass position vector and velocity vector

void get_cm_qty(double** myArray, vector<double> residueAtomicWeights, vector<int> atomNosForSelectedResidue, double* cm)
  {
  double totalMass = 0; 
  for ( int i = 0; i < 3; i++ )
    {
    cm[i] = 0;
    for ( int j = 0; j < atomNosForSelectedResidue.size(); j++ )
      {
      totalMass += residueAtomicWeights[j];
      cm[i] += myArray[atomNosForSelectedResidue[j]][i] * residueAtomicWeights[j];
      }
    cm[i] = cm[i] / totalMass;
    totalMass = 0;
    }
  }


// This function calculates the angle between two vectors. 

double getAngle(double** coordinatesArray_t, vector<double> residueAtomicWeights, vector<int> atomNosForSelectedResidue, int one, int two)
  {
  double angle = 0, dotProduct = 0, v1Mag = 0, v2Mag = 0;
  double* v1 = new double[3];
  double* v2 = new double[3];
  double* Rr = new double[3];
  get_cm_qty(coordinatesArray_t, residueAtomicWeights, atomNosForSelectedResidue, Rr);
  for ( int i = 0; i < 3; i++ )
    {
    v1[i] = Rr[i] - coordinatesArray_t[one][i];
    v2[i] = coordinatesArray_t[two][i] - coordinatesArray_t[one][i];
    }
  
  for ( int i = 0; i < 3; i++ )
    {
    dotProduct += v1[i] * v2[i];
    v1Mag += v1[i] * v1[i];
    v2Mag += v2[i] * v2[i];
    }

  v1Mag = sqrt(v1Mag);
  v2Mag = sqrt(v2Mag); 

  cout << "Dot product " << dotProduct << " mag 1 = " << v1Mag << " mag2 = " << v2Mag << endl; 

  angle = acos ( dotProduct / ( v1Mag * v2Mag ) );

  //cout << angle << endl;

  return angle;
  }


// This function determines the flag of whether the residue is behind the donor or the acceptor atom and returns a flag 0 if it is behind the donor and 1 if it is behind the acceptor


int determineFlag(double** coordinatesArray_t, vector<double> residueAtomicWeights, vector<int> atomNosForSelectedResidue, int a, int d)
  {
  double angle_RDA = 0, angle_RAD = 0;
  int flag = 0;
/*
  for (int i = 0; i < residueAtomicWeights.size(); i++)
    {
    cout << residueAtomicWeights[i] << endl;
    }
*/
    
  angle_RDA = getAngle(coordinatesArray_t, residueAtomicWeights, atomNosForSelectedResidue, d, a);
  angle_RAD = getAngle(coordinatesArray_t, residueAtomicWeights, atomNosForSelectedResidue, a, d);

  cout << "angle_RDA = " << angle_RDA << " angle_RAD = " << angle_RAD << endl;  


  if (angle_RDA > M_PI / 2)
    {
    flag = 0;
    count0++;
    }
  else if (angle_RAD > M_PI / 2)
    {
    flag = 1;
    count1++;
    }
  else
    {
    flag = 2;
    count2++;
    }

  cout << flag << endl;

  //cout << "count0 = " << count0 << " count1 = " << count1 << " count2 = " << count2 << endl;

  return flag;
  }





// This function calculates the directional component.
// Note: Here, r is the resiude number, while a and d are atom numbers of the donor and acceptor.
//
double calculateDirectionalComponent(double** velocityArray_t, double** coordinatesArray_t, int* CA_vector, vector<double> atomicWeights_r, vector<int> atomNosForAResidue_r, int d, int a, int r, int flag, int t)
  {
  double dirComp = 0, normalizingFactor_RD = 0, normalizingFactor_DA = 0, normalizingFactor_AD = 0, normalizingFactor_RA = 0, term1 = 0, term2 = 0;
  double* Vr  = new double[3];
  double* Rr  = new double[3];
  double* Rrd = new double[3];  // This is used in case 1 where the residue is behind the donor
  double* Rda = new double[3];  
  double* Rad = new double[3];  
  double* Rra = new double[3];  // This will be used in case 2 where the residue is behind the acceptor
  double* Urd = new double[3];
  double* Uda = new double[3];
  double* Uad = new double[3];
  double* Ura = new double[3];

  get_cm_qty(velocityArray_t, atomicWeights_r, atomNosForAResidue_r, Vr);
  get_cm_qty(coordinatesArray_t, atomicWeights_r, atomNosForAResidue_r, Rr);

  if (flag == 0)
    {
    
    for (int i = 0; i < 3; i++)
      {
      Rrd[i] = -( Rr[i] - coordinatesArray_t[d][i] );
      Rda[i] = coordinatesArray_t[a][i] - coordinatesArray_t[d][i];
      }

    for (int i = 0; i < 3; i++)
      {
      normalizingFactor_RD += Rrd[i] * Rrd[i];
      normalizingFactor_DA += Rda[i] * Rda[i];
      }

    normalizingFactor_RD = sqrt(normalizingFactor_RD);
    normalizingFactor_DA = sqrt(normalizingFactor_DA);

    for (int i = 0; i < 3; i++)
      {
      Urd[i] = Rrd[i] / normalizingFactor_RD;
      Uda[i] = Rda[i] / normalizingFactor_DA;
      }

    for (int i = 0; i < 3; i++)
      {
      term1 += Vr[i] * Urd[i];
      term2 += Urd[i] * Uda[i];
      } 

    dirComp = term1 * term2;

    /*
    if (dirComp == 0)
      {
      cout << r << endl;
      }
    */

    return dirComp;

    }
  else if (flag == 1)
    {
    
    for (int i = 0; i < 3; i++)
      {
      Rra[i] = -( Rr[i] - coordinatesArray_t[a][i] );
      Rad[i] = coordinatesArray_t[d][i] - coordinatesArray_t[a][i];      
      }

    for (int i = 0; i < 3; i++)
      {
      normalizingFactor_RA += Rra[i] * Rra[i];
      normalizingFactor_AD += Rad[i] * Rad[i];
      }
    
    normalizingFactor_RD = sqrt(normalizingFactor_RD);
    normalizingFactor_AD = sqrt(normalizingFactor_AD);


    for (int i = 0; i < 3; i++)
      {
      Ura[i] = Rra[i] / normalizingFactor_RA;
      Uad[i] = Rad[i] / normalizingFactor_AD;
      }

    for (int i = 0; i < 3; i++)
      {
      term1 += Vr[i] * Ura[i];
      term2 += Ura[i] * Uad[i];
      } 

    dirComp = term1 * term2;
/*
    if (abs(dirComp) < pow(10.0, -10.0))
      {
      cout << "Residue " << r << " has 0 directional component at time " << t << endl;
      cout << "This is because of term 1 " << term1 << " or term2 " << term2 << endl; 
      }
*/
    return dirComp;

    }

  else if ( flag == 2 )
    {
    dirComp = 0;
    return dirComp;
//    cout << "Residue " << r << " is between the donor and acceptor at time " << t << endl;
    }

  else
    {
    cout << "Wrong flag. Flag needs to be 0, 1 or 2 depending on whether the residue is behind the donor or the acceptor or between them." << endl;
    }

  }









// This function creates the velocity array with all zeros.

/*
void initialize(double*** myArray, int nAtom, int trajLen)
    {
    for (int i = 0; i < trajLen; i++)
        {
        myArray[i] = new double*[nAtom];
        for (int j = 0; j < nAtom; j++)
            {
        	  myArray[i][j] = new double[3];
        	  for (int k = 0; k < 3; k++)
        		  {
            	myArray[i][j][k] = 0;
        		  }
            }
        }
    }
*/


/*
void deleteArray(double*** myArray, int trajLen, int nAtom)
  {
  for (int i = 0; i < trajLen; i++)
    {
    for (int j = 0; j < nAtom; j++)
      {
      delete [] myArray[i][j];
      }
    delete [] myArray[i];
    }
  delete [] myArray;
  }
*/




int main()
  {
  // Declaring the variables.
  int nAtom = 3986;
  int trajLen = 3000;
  int nRes = 254;
  int a = 1585;
  int d = 3978;
  const char* mdvelFileName = "/Users/Saurabh/workFiles/KE07/300ps_vel/KE07_lig/run_1/trajectories/WT/WT_lig_300ps_noWat.mdvel";
  const char* mdcrdFileName = "/Users/Saurabh/workFiles/KE07/300ps_vel/KE07_lig/run_1/trajectories/WT/WT_lig_300ps_noWat.mdcrd";
  const char* pdbFileName = "/Users/Saurabh/workFiles/KE07/300ps_vel/KE07_lig/run_1/trajectories/WT/WT_lig.pdb";

  // Declaring internal variables
  double*** velocityArray;
  double*** coordinatesArray;
  int* CA_vector = new int[nRes];
  int** atomNoVector = new int*[nRes];
  double A = 0, B = 0;
  int flag = 0, resNo = 0, counter = 0;
  size_t foundC;
  size_t foundO;
  size_t foundN;
  size_t foundS;
  vector<int> atomNosForAResidue[nRes];
  vector<string> atomTypesForAResidue[nRes];
  vector<double> atomicWeights[nRes];


  // Map CA atom number to residue number as well as have an array mapping a list of all atom numbers in a particular residue to the residue number. 

  string pdbLine;
  ifstream pdbFile (pdbFileName);

  if ( pdbFile.is_open() )
    {
    while ( pdbFile.good() )
      {
      getline ( pdbFile,pdbLine );

      vector<string> strs;
      boost::trim(pdbLine);
      boost::split(strs, pdbLine, boost::is_any_of(" "), boost::token_compress_on);

      if ( strs.size() == 10 )
        {
        resNo = atoi( strs[4].c_str() );
        atomNosForAResidue[resNo-1].push_back( atoi(strs[1].c_str()) );
        atomTypesForAResidue[resNo-1].push_back( strs[2] );
        if ( strs[2] == "CA" )
          {
          counter = atoi( strs[4].c_str() );
          CA_vector[counter] = atoi( strs[1].c_str() );          
          }
        }
      }      
      pdbFile.close();
    }
  else
    {
    cout << "Unable to open file";
    pdbFile.close();
    }


  // Creates an array with the values of atomic weights 

  for ( int i = 0; i < nRes; i++ )
    {
    for ( int j = 0; j < atomNosForAResidue[i].size(); j++ )
      {
      foundC = atomTypesForAResidue[i][j].find("C");
      foundO = atomTypesForAResidue[i][j].find("O");
      foundN = atomTypesForAResidue[i][j].find("N");
      foundS = atomTypesForAResidue[i][j].find("S");
      if ( foundC != string::npos )
        {
        atomicWeights[i].push_back(12.0);
        }
      else if ( foundO != string::npos )
        {
        atomicWeights[i].push_back(16.0);
        }
      else if ( foundN != string::npos )
        {
        atomicWeights[i].push_back(14.0);
        }
      else if ( foundS != string::npos )
        {
        atomicWeights[i].push_back(32.0);
        }
      else
        {
        atomicWeights[i].push_back(1.0);
        }
      }
    }



  // Calling the functions.
//  velocityArray = new double**[trajLen];
//  coordinatesArray = new double**[trajLen];

//  initialize(velocityArray, nAtom, trajLen);

//  initialize(coordinatesArray, nAtom, trajLen);


  // Declare velocities array
  velocityArray = new double**[trajLen];
  for (int i = 0; i < trajLen; i++)
    {
    velocityArray[i] = new double*[nAtom];
    for (int j = 0; j < nAtom; j++)
      {
      velocityArray[i][j] = new double[3];
      for (int k = 0; k < 3; k++)
        {
        velocityArray[i][j][k] = 0;
        }
      }
    }


  // Declare coordinate array
  coordinatesArray = new double**[trajLen];
  for (int i = 0; i < trajLen; i++)
    {
    coordinatesArray[i] = new double*[nAtom];
    for (int j = 0; j < nAtom; j++)
      {
      coordinatesArray[i][j] = new double[3];
      for (int k = 0; k < 3; k++)
        {
        coordinatesArray[i][j][k] = 0;
        }
      }
    }

  cout << "Hi Hello How are you" << endl;

  // Read in velocity Array
  readInArray(velocityArray, mdvelFileName, nAtom);

  // Read in coordinate Array
  readInArray(coordinatesArray, mdcrdFileName, nAtom);

  cout << "Hi Hello How are you" << endl;

  char* donorAcceptorFileName = (char*) "/Users/Saurabh/workFiles/KE07/300ps_vel/KE07_lig/run_1/pairwiseVelocities/WT/donor_acceptor.vel";
  ofstream donorAcceptorFile;
  donorAcceptorFile.open(donorAcceptorFileName);


  for (int t = 0; t < trajLen; t++)
    {
    A = calculatePPV(velocityArray, coordinatesArray, a, d, t);
    donorAcceptorFile << A << endl;
    }


    donorAcceptorFile.close();


  
  for (int r = 0; r < nRes-1; r++)
    {
    string outputFile = "/Users/Saurabh/workFiles/KE07/300ps_vel/KE07_lig/run_1/pairwiseVelocities/WT/";
    //cout << "Hi " << r << endl;

    outputFile.append(boost::lexical_cast<string>(r+1));
    outputFile.append(".vel");

    char* outputFileName = (char*) outputFile.c_str();

    ofstream B_OutputFile;

    B_OutputFile.open(outputFileName);

    for (int t = 0; t < trajLen; t++)
      {
      cout << "Hi " << r << " " << t << endl;
      flag = determineFlag(coordinatesArray[t], atomicWeights[r], atomNosForAResidue[r], a, d);
      B = calculateDirectionalComponent(velocityArray[t], coordinatesArray[t], CA_vector, atomicWeights[r], atomNosForAResidue[r], d, a, r, flag, t);

//      if (abs(B) < pow(10.0, -10.0))
//        {
//        cout << "The directional component for residue " << r << " at time " << t << " is " << B << endl;
//        }


      B_OutputFile << B << endl;
      }

    B_OutputFile.close();

    }
 


  //deleteArray(velocityArray, trajLen, nAtom);

  for (int i = 0; i < trajLen; i++)
    {
    for (int j = 0; j < nAtom; j++)
      {
      delete [] velocityArray[i][j];
      }
    delete [] velocityArray[i];
    }
  delete [] velocityArray;

  for (int i = 0; i < trajLen; i++)
    {
    for (int j = 0; j < nAtom; j++)
      {
      delete [] coordinatesArray[i][j];
      }
    delete [] coordinatesArray[i];
    }
  delete [] coordinatesArray;




  return 0;
  }
