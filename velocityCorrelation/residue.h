/*
 * residue.h
 *
 *  Created on: Nov 1, 2011
 *      Author: Saurabh
 */
#include<iostream>
#include<string>
#include<vector>
#include<map>
#include<utility>
#include"atom.h"

using namespace std;


#ifndef RESIDUE_H_
#define RESIDUE_H_

class residue {
public:
	residue();
	residue(string residueType, int residueNumber);
	virtual ~residue();
	map<string, atom*> atomObject;
	vector<string> heavyAtoms;
	vector<double> dihedralAngles;
	void setResidueType(string residueType){this->residueType = residueType;}
	string getResidueType(void) {return this->residueType;}
	void setResidueNumber(int residueNumber) {this->residueNumber =  residueNumber;}
	int getResidueNumber(void) {return this->residueNumber;}
	void getHeavyAtoms(void);

private:
	string residueType;
	int residueNumber;

};

#endif /* RESIDUE_H_ */
