/*
 Calculating the velocity correlation functions.
 This code will calculate the velocity correlation function between velocities of residues from the mdvel files. 
*/

// Includes
#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <time.h>
#include <vector>
#include <boost/algorithm/string.hpp>
#define _USE_MATH_DEFINES


using namespace std;

// Defining the functions.

void readInArray(double*** myArray, const char* dataFileName, int nAtom)
    {
    int i = 0, j = 0, k = 0, prevLen = 10;
    string line;
    ifstream dataFile (dataFileName);

    if (dataFile.is_open())
      {
      while ( dataFile.good() )
        {
        getline (dataFile,line);
        //cout << line << endl;
        
        vector<string> strs;
        boost::trim(line);
        boost::split(strs, line, boost::is_any_of(" "), boost::token_compress_on);

        if ( strs.size() == 10 || prevLen == 10 )
          {
          for (int l = 0; l < strs.size(); l++)
            {
            //cout << "l  " << l << " strs  " << atof(strs[l].c_str()) << endl;
            //cout << "i = " << i << " j = " << j << " k = " << k << endl;
            myArray[i][j][k] = atof(strs[l].c_str());
            //cout << " myArray[i][j][k] = " << myArray[i][j][k] << endl;          
            k++;
            if ( k == 3 )
              {
              j++;
              k = 0;
              }
            if ( j > 0 && j % nAtom == 0)
              {
              j = 0;
              i++;
              }
            }
          }
        prevLen = strs.size();
        strs.clear();
        }
      dataFile.close();
      }

    else
      {
      cout << "Unable to open file";
      dataFile.close();
      }
    }


// This function calculates the PPV
double calculatePPV(double*** velocityArray, double*** coordinatesArray, int a, int d, int t)
  {
  double PPV = 0, normalizingFactor = 0;
  double* Vda = new double[3];
  double* Rda = new double[3];
  double* Uda = new double[3];

  for (int i = 0; i < 3; i ++)
    {
    Vda[i] = velocityArray[t][d][i] - velocityArray[t][a][i];
    Rda[i] = coordinatesArray[t][d][i] - coordinatesArray[t][a][i];
    normalizingFactor += Rda[i] * Rda[i];
    }

  normalizingFactor = sqrt(normalizingFactor);

  for (int i = 0; i < 3; i++)
    {
    Uda[i] = Rda[i] / normalizingFactor;
    }

  for (int i = 0; i < 3; i++)
    {
    PPV += Vda[i] * Uda[i];
    }

  return PPV;
  }



// This function calculates the center of mass position vector and velocity vector

//void get_cm_qty(double*** myArray, double* cm)
//  {
//  Still to be implemented
//  }





// This function calculates the directional component. Currently I am using C-alpha for R instead of the center of mass of that residue.
//
// Note: Here, r is the resiude number, while a and d are atom numbers of the donor and acceptor.
//
double calculateDirectionalComponent(double*** velocityArray, double*** coordinatesArray, int* CA_vector, int d, int a, int r, int t, int flag)
  {
  double dirComp = 0, normalizingFactor_RD = 0, normalizingFactor_DA = 0, normalizingFactor_RA = 0, term1 = 0, term2 = 0;
  double* Vr  = new double[3];
  double* Rr  = new double[3];
  double* Rrd = new double[3];  // This is used in case 1 where the residue is behind the donor
  double* Rda = new double[3];  
  double* Rra = new double[3];  // This will be used in case 2 where the residue is behind the acceptor
  double* Urd = new double[3];
  double* Uda = new double[3];
  double* Ura = new double[3];

//  get_cm_qty(velocityArray, Vr);
//  get_cm_qty(coordinatesArray, Rr);

  if (flag == 0)
    {
    
    for (int i = 0; i < 3; i++)
      {
      Vr[i] = velocityArray[t][CA_vector[r]][i];
      Rr[i] = coordinatesArray[t][CA_vector[r]][i];
      Rrd[i] = coordinatesArray[t][CA_vector[r]][i] - coordinatesArray[t][d][i];
      Rda[i] = coordinatesArray[t][a][i] - coordinatesArray[t][d][i];
      }

    for (int i = 0; i < 3; i++)
      {
      normalizingFactor_RD += Rrd[i] * Rrd[i];
      normalizingFactor_DA += Rda[i] * Rda[i];
      }

    for (int i = 0; i < 3; i++)
      {
      Urd[i] = Rrd[i] / normalizingFactor_RD;
      Uda[i] = Rda[i] / normalizingFactor_DA;
      }

    for (int i = 0; i < 3; i++)
      {
      term1 += Vr[i] * Urd[i];
      term2 += Urd[i] * Uda[i];
      } 

    dirComp = term1 * term2;

    return dirComp;

    }
  else if (flag == 1)
    {
    cout << "Still to be implemented" << endl;
    }

  }









// This function creates the velocity array with all zeros.

/*
void initialize(double*** myArray, int nAtom, int trajLen)
    {
    myArray = new double**[trajLen];
    for (int i = 0; i < trajLen; i++)
        {
        myArray[i] = new double*[nAtom];
        for (int j = 0; j < nAtom; j++)
            {
        	  myArray[i][j] = new double[3];
        	  for (int k = 0; k < 3; k++)
        		  {
            	myArray[i][j][k] = 0;
        		  }
            }
        }
    }
*/


/*
void deleteArray(double*** myArray, int trajLen, int nAtom)
  {
  for (int i = 0; i < trajLen; i++)
    {
    for (int j = 0; j < nAtom; j++)
      {
      delete [] myArray[i][j];
      }
    delete [] myArray[i];
    }
  delete [] myArray;
  }
*/




int main()
  {
  // Declaring the variables.
  int nAtom = 3987;
  int trajLen = 3000;
  int nRes = 253;
  int a = 1586;
  int d = 3979;
  const char* mdvelFileName = "/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_apo/run_1/velocityCorrelations_30ps_10pc/velCorr/WT_lig_30ps_10fs_noWat.mdvel";
  const char* mdcrdFileName = "/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_apo/run_1/velocityCorrelations_30ps_10pc/velCorr/WT_lig_30ps_10fs_noWat.mdcrd";
  const char* pdbFileName = "/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_apo/run_1/velocityCorrelations_30ps_10pc/velCorr/WT_lig_30ps_noWat.pdb";
  const char* outputFile = "/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_apo/run_1/velocityCorrelations_30ps_10pc/velCorr/A_B.txt";

  // Declaring internal variables
  double*** velocityArray;
  double*** coordinatesArray;
  int* CA_vector = new int[nRes];
  int counter = 0;
  double A = 0, B = 0;
  int flag = 0;

  // Map CA atom number to residue number

  string pdbLine;
  ifstream pdbFile (pdbFileName);


  if (pdbFile.is_open())
    {
    while ( pdbFile.good() )
      {
      //counter = counter + 1;
      getline ( pdbFile,pdbLine );
      //cout << pdbLine << endl;
      
      vector<string> strs;
      boost::trim(pdbLine);
      boost::split(strs, pdbLine, boost::is_any_of(" "), boost::token_compress_on);

      if ( strs.size() == 10 && strs[2] == "CA" )
        {
        counter = atoi(strs[4].c_str());
        CA_vector[counter] = atoi(strs[1].c_str());
        cout << counter << "\t" << CA_vector[counter] << endl;
        }
      }      
      pdbFile.close();
    }
  else
    {
    cout << "Unable to open file";
    pdbFile.close();
    }
 
 






  // Calling the functions.

  //initialize(velocityArray, nAtom, trajLen);

  // Declare velocities array
  velocityArray = new double**[trajLen];
  for (int i = 0; i < trajLen; i++)
    {
    velocityArray[i] = new double*[nAtom];
    for (int j = 0; j < nAtom; j++)
      {
      velocityArray[i][j] = new double[3];
      for (int k = 0; k < 3; k++)
        {
        velocityArray[i][j][k] = 0;
        }
      }
    }


  // Declare coordinate array
  coordinatesArray = new double**[trajLen];
  for (int i = 0; i < trajLen; i++)
    {
    coordinatesArray[i] = new double*[nAtom];
    for (int j = 0; j < nAtom; j++)
      {
      coordinatesArray[i][j] = new double[3];
      for (int k = 0; k < 3; k++)
        {
        coordinatesArray[i][j][k] = 0;
        }
      }
    }

  // Read in velocity Array
  readInArray(velocityArray, mdvelFileName, nAtom);

  // Read in coordinate Array
  readInArray(coordinatesArray, mdcrdFileName, nAtom);


  ofstream A_B_OutputFile;

  A_B_OutputFile.open(outputFile);
  
  int r = 202;

  for (int t = 0; t < trajLen; t++)
    {
    A = calculatePPV(velocityArray, coordinatesArray, a, d, t);
    B = calculateDirectionalComponent(velocityArray, coordinatesArray, CA_vector, d, a, r, t, flag);

    A_B_OutputFile << A << "\t" << B << endl;
    }


  A_B_OutputFile.close();


  //deleteArray(velocityArray, trajLen, nAtom);

  for (int i = 0; i < trajLen; i++)
    {
    for (int j = 0; j < nAtom; j++)
      {
      delete [] velocityArray[i][j];
      }
    delete [] velocityArray[i];
    }
  delete [] velocityArray;

  for (int i = 0; i < trajLen; i++)
    {
    for (int j = 0; j < nAtom; j++)
      {
      delete [] coordinatesArray[i][j];
      }
    delete [] coordinatesArray[i];
    }
  delete [] coordinatesArray;




  return 0;
  }
