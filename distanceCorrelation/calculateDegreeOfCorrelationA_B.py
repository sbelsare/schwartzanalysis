'''
Created on Aug 23, 2012

@author: Saurabh

This will calculate the degree of correlation, i.e. the amplitude at the frequency where the donor acceptor autocorrelation has its maxima

'''

import math, numpy, sys, os
from numpy import *


if __name__ == '__main__':

    # Low and high denote the range between which the frequencies are observed, i.e. around the maxima of the donor acceptor autocorrelation. 

    low = 150
    high = 190

#    low1 = 163
#    high1 = 173
#    low2 = 173
#    high2 = 185
#    low3 = 205
#    high3 = 215
#    low4 = 225
#    high4 = 234
#    low5 = 234
#    high5 = 240
#    low6 = 
#    high6 = 






    num1 = 101
    proteinLength = 254


	#runSet = ["run_1", "run_2", "run_3", "run_4", "run_5", "run_7", "run_8"]
    runSet = ["run_1"]

    for run in runSet:

        #roundSet = ["WT", "R2", "R3", "R4", "R5", "R6", "R7-1", "R7-2"]
        roundSet = ["WT", "R2", "R3", "R4", "R5", "R6", "R7-2"]
        #roundSet = ["R6"]

        for roundNo in roundSet:

			### This is r-50, 50-101

            #f = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/degCorr/'+roundNo+'/degreeOfCorrelation_donor_acceptor_indexes.degCorr', 'w')
            f = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/degCorr/'+roundNo+'/degreeOfCorrelation_donor_acceptor_lowFreq.degCorr', 'w')

            for i in range(1, proteinLength):

                if i!=num1:

                    inFile = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/FFTOutput/'+roundNo+'/fourierTransformed_'+str(i)+'.fft', 'r')

                    fftVector = []

                    for line in inFile:
						fftVector.append(float(line.split()[0]))
                
                shortFFTVector = []
        
                for j in range(low, high):
                    shortFFTVector.append(fftVector[j])


                if i == num1:
					f.write(str(i)+"    "+str(0) + "\n")
                else:
					#f.write(str(i)+" "+str(fftVector.index(max(fftVector))) + "\n")
					f.write(str(i)+" "+str(max(shortFFTVector)) + "\n")

					inFile.close()


            f.close()
		
	pass

    	
