'''
Created on Aug 23, 2012

@author: Saurabh

This function calculates the distance correlation for a particular trio
of residues where distanceVector1 is the vector between residues 1 and 2
and distanceVector2 is the vector between residues 2 and 3.
If distanceVector1 and distanceVector2 are equal, this function will return the autocorrelation.

This function is normalized in such a way as to have mean zero and stddev 1. 
The distance vectors, however, are not normalized to start at 1. 

@args: distanceVector1, distanceVector2 (The vector of pairwise distances
as functions of time). Both these vectors need to be column vectors.

@returns: The value of the distance cross correlation for the 3 residues being considered.

'''

import math, numpy, sys, os
from numpy import *

def calculateVelocityCorrelation(velocityVector1, velocityVector2):
    L = min(len(velocityVector1), len(velocityVector2))

    velCorr1 = [0]*(int(L/10))
    velCorr2 = [0]*(int(L/10))
    velCorr  = [0]*(int(L/10))

    mean1   = mean(velocityVector1)
    mean2   = mean(velocityVector2)
    stddev1 = sqrt(var(velocityVector1))
    stddev2 = sqrt(var(velocityVector2))

    velocityVector1 = velocityVector1-mean1
    velocityVector2 = velocityVector2-mean2
    velocityVector1 = velocityVector1/stddev1
    velocityVector2 = velocityVector2/stddev2

    velocityVector1 = matrix(velocityVector1)
    velocityVector2 = matrix(velocityVector2)

    for k in range(0, int(L/10)):
        temp        = velocityVector1[0, k:L]*velocityVector2[0, 0:(L-k)].T
        velCorr1[k] = temp[0, 0]/(L-k)

    for l in range(0, int(L/10)):
        temp2       = velocityVector2[0, l:L]*velocityVector1[0, 0:(L-l)].T
        velCorr2[l] = temp2[0, 0]/(L-l)

    for m in range(0, int(L/10)):
	    velCorr[m] = (velCorr1[m] + velCorr2[m]) / 2

    return velCorr
 

if __name__ == '__main__':

    num1 = 101
    proteinLength = 254


    #roundSet = ["WT", "R2", "R3", "R4", "R5", "R6", "R7-1", "R7-2"]
    roundSet = ["WT", "R2", "R3", "R4", "R5", "R6", "R7-2"]
    #roundSet = ["WT"]

    for roundNo in roundSet:

        #runSet = ["run_1", "run_2", "run_3", "run_4", "run_5", "run_6", "run_7", "run_8", "run_9", "run_10", "run_11", "run_12", "run_13", "run_14", "run_15", "run_16", ]
        #runSet = ["run_6", "run_9", "run_10", "run_11", "run_12", "run_13", "run_14", "run_15", "run_16"]
        #runSet = ["run_1", "run_2", "run_3", "run_4", "run_5", "run_7", "run_8"]
        runSet = ["run_1"]

        for run in runSet:

            f0 = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/pairwiseVelocities/'+roundNo+'/donor_acceptor.vel', 'r')

            velocityVector1 = []

            for line in f0:
                velocityVector1.append(float(line.split()[0]))

            autoVelCorr = calculateVelocityCorrelation(velocityVector1, velocityVector1)

            nextPow2 = int(pow(2, ceil(math.log(len(autoVelCorr), 2))))

            myAutoVelCorrFFT = abs(numpy.fft.fft(autoVelCorr, nextPow2)/len(autoVelCorr))

            myAutoVelCorrFFTss = []

            myAutoVelCorrFFTss.append(myAutoVelCorrFFT[0])

            for counter in range(1, nextPow2/2+1):
                myAutoVelCorrFFTss.append(2*myAutoVelCorrFFT[counter])

            autoVelCorrFile = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/directionalCorr/'+roundNo+'/autoVelCorr_donor_acceptor.corr', 'w')
            for element in autoVelCorr:
                autoVelCorrFile.write(str(element)+"\n")
            autoVelCorrFile.close()
            
            autoVelCorrFftFile = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/FFTOutput/'+roundNo+'/fourierTransformed_donor_acceptor.fft', 'w')
            for element in myAutoVelCorrFFTss:
                autoVelCorrFftFile.write(str(element)+"\n")
            autoVelCorrFftFile.close()


            for i in range(1, proteinLength):
                
                f1 = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/pairwiseVelocities/'+roundNo+'/'+str(i)+'.vel', 'r')

                if i!=num1:

                    velocityVector2 = []
                
                    for line in f1:
                        velocityVector2.append(float(line.split()[0]))
                                
                    velCorr = calculateVelocityCorrelation(velocityVector1, velocityVector2)

                    nextPow2 = int(pow(2, ceil(math.log(len(velCorr), 2))))

                    myFFT = abs(numpy.fft.fft(velCorr, nextPow2)/len(velCorr))

                    myFFTss = []

                    myFFTss.append(myFFT[0])

                    for counter in range(1, nextPow2/2+1):
                        myFFTss.append(2*myFFT[counter])

                    velCorrFile = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/directionalCorr/'+roundNo+'/velCorr_'+str(i)+'.corr', 'w')
                    for element in velCorr:
                        velCorrFile.write(str(element)+"\n")
                    velCorrFile.close()

                    fftFile = open('/Users/Saurabh/workFiles/KE07/30ps_vel/KE07_lig/'+run+'/FFTOutput/'+roundNo+'/fourierTransformed_'+str(i)+'.fft', 'w')
                    for element in myFFTss:
                        fftFile.write(str(element)+"\n")
                    fftFile.close()

                f1.close()

    pass

